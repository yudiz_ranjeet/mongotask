const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  sUserName: {
    type: String,
    required: true,
    maxlength: 32,
    trim: true
  },
  sEmail: {
    type: String,
    trim: true,
    required: true,
    unique: true
  },
  nPhone: {
    type: Number,
    minlength: 10,
    maxlength: 10,
    required: true
  },
  sGender: {
    type: String,
    required: true
  },
  sPassword: {
    type: String,
    required: true
  },
  iImage: {
    data: Buffer,
    type: String,
    required: true

  },
  token: {
    type: String,
    default: ''
  },
  salt: String
}, { timestamps: true })

module.exports = mongoose.model('User', userSchema)
