const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
  sTitle: {
    type: String,
    required: true,
    unique: true
  },
  sDesc: {
    type: String,
    required: true
  },
  iCoverImage: {
    data: Buffer,
    type: String,
    required: true
  },
  bPublished: {
    type: Boolean,
    required: true
  },
  sUserID: {
    type: String,
    required: true
  },
  isLike: {
    type: Array,
    default: 0
  }
}, { timestamps: true })

module.exports = mongoose.model('Post', PostSchema)
