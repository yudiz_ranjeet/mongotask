const mongoose = require('mongoose')

const adminSchema = new mongoose.Schema({
  sAdminName: {
    type: String,
    required: true,
    maxlength: 32,
    trim: true
  },
  sEmail: {
    type: String,
    trim: true,
    required: true,
    unique: true
  },
  sPassword: {
    type: String,
    required: true
  },
  token: {
    type: String,
    default: ''
  },
  sUserType: {
    type: String
  },
  salt: String
}, { timestamps: true })

module.exports = mongoose.model('Admin', adminSchema)
