const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const cors = require('cors')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileUpload')

mongoose.connect('mongodb://localhost:27017/profile')

const db = mongoose.connection
db.on('error', (err) => {
  console.log(err)
})
db.once('open', () => {
  console.log('connection successful with db')
})

const app = express()
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use(fileUpload({
  useTempFiles: true
}))
const PORT = process.env.PORT || 8080

const userRoutes = require('./router/user')
const blog = require('./router/blog')
const admin = require('./router/admin')

app.use('/api', userRoutes)
app.use('/api/blog', blog)
app.use('/api/admin', admin)
app.listen(PORT, () => {
  console.log('server is running on port', PORT)
})
