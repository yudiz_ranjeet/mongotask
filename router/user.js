const express = require('express')
const router = express.Router()
const user = require('../controllers/user')

router.post('/signup', user.signup)

router.post('/login', user.login)
// router.post("/userProfile", user.userProfile)
router.post('/logout', user.LogOut)

router.post('/update', user.update)

router.post('/updatePassword', user.updatePassword)
router.post('/changePassword', user.changePassword)
router.post('/sendOtp', user.sendOtp)
module.exports = router
