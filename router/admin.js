const express = require('express')
const router = express.Router()
const admin = require('../controllers/admin')

router.post('/signup', admin.signup)
router.post('/login', admin.login)
router.get('/showBlog/:id', admin.showBlog)
router.post('/deleteUser/:id', admin.deleteUser)
router.get('/users/:id', admin.users)
module.exports = router
