const express = require('express')
const router = express.Router()
const blog = require('../controllers/blog')

router.post('/postBlog', blog.postBlog)
router.get('/showBlog', blog.showBlog)
router.post('/like', blog.like)

module.exports = router
