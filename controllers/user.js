const User = require('../module/user')
const { validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const nodemailer = require('nodemailer')
const Otp = require('../module/otp')

const cloudinary = require('cloudinary').v2

cloudinary.config({
  cloud_name: 'raj-yudiz',
  api_key: '858292898849192',
  api_secret: 'DrOObnclzx_C9l0ovSdmV07XOQM',
  secure: true
})

/* .............Signup Login........... */
const signup = (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({
      error: errors.array()[0].msg
    })
  }

  bcrypt.hash(req.body.sPassword, 10, (err, hash) => {
    if (err) {
      return res.status(400).json({
        error: err
      })
    } else {
      const file = req.files.image
      cloudinary.uploader.upload(file.tempFilePath, (_err, result) => {
        const user = new User({
          sUserName: req.body.sUserName,
          sEmail: req.body.sEmail,
          nPhone: req.body.nPhone,
          sGender: req.body.sGender,
          sPassword: hash,
          iImage: result.url
        })
        user.save()
          .then(result => {
            res.status(200).json({
              new_user: result
            })
          })
          .catch(_err => {
            res.status(400).json({
              message: 'oops!!! wrong details please check'
            })
          })
      })
    }
  })
}

const login = (req, res) => {
  User.find({ sEmail: req.body.sEmail })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: 'user not exist'
        })
      }
      bcrypt.compare(req.body.sPassword, user[0].sPassword, (err, result) => {
        if (!result) {
          return res.status(401).json({
            message: 'password wrong'
          })
        }
        if (result) {
          const token = jwt.sign({
            sUserName: user[0].sUserName,
            sEmail: user[0].sEmail,
            nPhone: user[0].nPhone,
            sGender: user[0].sGender
          }, 'your details', {
            expiresIn: '24h'
          })
          User.findOneAndUpdate({ sEmail: user[0].sEmail }, { $set: { token } })
            .then(response => {
              res.json({
                sUserName: user[0].sUserName,
                sEmail: user[0].sEmail,
                nPhone: user[0].nPhone,
                sGender: user[0].sGender,
                sImage: user[0].iImage,
                _id: user[0]._id,
                token
              })
            })
            .catch(_err => {
              res.json({
                error: err
              })
            })
        }
      })
    })
    .catch(_err => {
      res.status(400).json({
        message: 'Oops!!! wrong id check your email id'
      })
    })
}

/* .............Update Information........... */
const update = (req, res, next) => {
  const userId = req.body.userID
  const updateData = {
    sUserName: req.body.sUserName,
    sEmail: req.body.sEmail,
    nPhone: req.body.nPhone,
    sGender: req.body.sGender
  }
  User.findByIdAndUpdate(userId, { $set: updateData })
    .then(response => {
      res.json({
        message: 'Update data Success'
      })
    })
    .catch(_err => {
      res.json({
        message: 'wrong details please provide correct data'
      })
    })
}

const updatePassword = (req, res, next) => {
  const userId = req.body.userID
  User.find({ _id: userId })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: 'user not exist'
        })
      }
      bcrypt.compare(req.body.sPassword, user[0].sPassword, (_err, result) => {
        if (!result) {
          return res.status(401).json({
            message: 'password wrong'
          })
        }
        if (result) {
          bcrypt.hash(req.body.sNewPassword, 10, (err, hash) => {
            if (err) {
              return res.status(400).json({
                error: err
              })
            } else {
              const updatePassword = {
                sPassword: hash
              }
              User.findByIdAndUpdate(userId, { $set: updatePassword })
                .then(response => {
                  res.json({
                    message: 'Update Password'
                  })
                })
                .catch(_err => {
                  res.json({
                    message: 'wrong information'
                  })
                })
            }
          })
        }
      })
    })
    .catch(err => {
      res.status(400).json({
        error: err
      })
    })
}

/* ............Forgot Password........... */
const sendOtp = (req, res, next) => {
  const Email = req.body.email
  User.findOne({ sEmail: Email })
    .exec()
    .then(user => {
      const otpCode = Math.floor((Math.random() * 10000) + 1)
      sendEmail(Email, otpCode)
      const otpData = new Otp({
        email: user.sEmail,
        code: otpCode,
        expiresIn: new Date().getTime() + 150 * 1000
      })
      otpData.save()
        .then(result => {
          res.status(200).json({
            message: 'otp send in your Email'
          })
        })
        .catch(err => {
          res.status(400).json({
            error: err
          })
        })
    })
    .catch(_err => {
      res.json({
        message: 'invalid user'
      })
    })
}

const sendEmail = async(email, otp) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'poweruniverse44@gmail.com',
      pass: 'harekrishna*2'
    }
  })

  const mailOptions = {
    from: 'poweruniverse44@gmail.com',
    to: email,
    subject: 'your Otp',
    text: 'otp' + otp
  }

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error)
    } else {
      console.log('Email sent: ' + info.response)
    }
  })
}

const changePassword = (req, res, next) => {
  const Email = req.body.email
  const otpCode = req.body.otp
  const newPassword = req.body.nPassword
  Otp.findOne({ code: otpCode })
    .exec()
    .then(use => {
      if (use.email === Email) {
        User.findOne({ sEmail: Email })
          .exec()
          .then(user => {
            bcrypt.hash(newPassword, 10, (err, hash) => {
              if (err) {
                return res.status(400).json({
                  error: err
                })
              } else {
                User.findOneAndUpdate({ sEmail: req.body.email }, { $set: { sPassword: hash } })
                  .exec()
                  .then(result => {
                    res.json({
                      message: 'password change'
                    })
                  })
                  .catch(_err => {
                    res.json({
                      message: 'password not change'
                    })
                  })
              }
            })
          })
          .catch(_err => {
            res.status(400).json({
              message: 'wrong user'
            })
          })
      } else {
        res.status(400).json({
          message: 'wrong otp user'
        })
      }
    })
    .catch(_err => {
      res.status(400).json({
        message: 'wrong otp'
      })
    })
}

/* .............User LogOut........... */
const LogOut = (req, res, next) => {
  const id = req.body.userID
  User.findById(id)
    .exec()
    .then(user => {
      if (user.token === '') {
        return res.status(400).json({
          message: 'user not login'
        })
      } else {
        User.findOneAndUpdate({ _id: id }, { $set: { token: '' } })
          .then(result => {
            res.json({
              message: 'logout'
            })
          })
          .catch(_err => {
            res.json({
              message: 'wrong information'
            })
          })
      }
    })
    .catch(_err => {
      res.json({
        message: 'user not found'
      })
    })
}

/* .............Export Data from user........... */
module.exports = {
  signup,
  login,
  LogOut,
  update,
  updatePassword,
  changePassword,
  sendOtp
}
