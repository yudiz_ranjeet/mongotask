const Admin = require('../module/admin')
const { validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const Blog = require('../module/blog')
const User = require('../module/user')

/* ....................for Admin.................... */
const signup = (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({
      error: errors.array()[0].msg
    })
  }
  Admin.find()
    .exec()
    .then(admin => {
      if (admin.length === 1) {
        return res.status(401).json({
          message: 'Admin already created'
        })
      }
      const hashedPass = bcrypt.hashSync(req.body.sPassword, 10)
      const data = new Admin({
        sAdminName: req.body.sAdminName,
        sEmail: req.body.sEmail,
        sPassword: hashedPass,
        sUserType: req.body.sUserType
      })
      data.save()
        .then(result => {
          res.status(200).json({
            new_user: result
          })
        })
        .catch(err => {
          res.status(400).json({
            error: err
          })
        })
    })
}

const login = (req, res) => {
  Admin.find({ sEmail: req.body.sEmail })
    .exec()
    .then(admin => {
      if (admin.length < 1) {
        return res.status(401).json({
          message: 'user not exist'
        })
      }
      bcrypt.compare(req.body.sPassword, admin[0].sPassword, (_err, result) => {
        if (!result) {
          return res.status(401).json({
            message: 'password wrong'
          })
        }
        if (result) {
          const token = jwt.sign({
            sAdminName: admin[0].sAdminName,
            sEmail: admin[0].sEmail
          }, 'your details', {
            expiresIn: '24h'
          })
          Admin.findOneAndUpdate({ sEmail: admin[0].sEmail }, { $set: { token } })
            .then(response => {
              res.json({
                sAdminName: admin[0].sAdminName,
                sEmail: admin[0].sEmail,
                _id: admin[0]._id,
                token
              })
            })
            .catch(_err => {
              res.json({
                message: 'OOps please check your details Something went wrong'
              })
            })
        }
      })
    })
    .catch(_err => {
      res.status(400).json({
        message: 'Admin not found'
      })
    })
}

/* ...................User Details................... */
const users = (req, res, next) => {
  const id = req.params.id
  Admin.find()
    .exec()
    .then(admin => {
      if (admin[0]._id === id) {
        const { page = 1, limit = 5, userID = 1 } = req.query
        User.find()
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec()
          .then(user => {
            if (userID === 1) {
              res.status(200).json({ total: user.length, user })
            } else {
              User.findById(userID)
                .exec()
                .then(result => {
                  res.status(200).json({ result })
                })
                .catch(_error => {
                  res.json({
                    message: 'user not found please check userID again'
                  })
                })
            }
          })
          .catch(_err => {
            res.json({
              message: 'User not found'
            })
          })
      } else {
        return res.status(401).json({
          message: 'wrong admin id'
        })
      }
    })
    .catch(_err => {
      res.json({
        message: 'An error Occurred?'
      })
    })
}

/* ...................Blog Details.................... */
const showBlog = (req, res, next) => {
  const id = req.params.id
  Admin.find()
    .exec()
    .then(admin => {
      if (admin[0]._id === id) {
        const { page = 1, limit = 5, isPublished = true, title = '', desc = '', id = '' } = req.query
        Blog.find({ bPublished: isPublished })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec()
          .then(blogs => {
            if (title !== '' || desc !== '' || id !== '') {
              Blog.find({ $or: [{ sTitle: title }, { sDesc: desc }, { sUserID: id }] })
                .exec()
                .then(blog => {
                  res.status(200).json({ total: blog.length, blog })
                })
                .catch(_err => {
                  res.status(401).json({
                    message: 'no blog present'
                  })
                })
            } else {
              res.status(200).json({ total: blogs.length, blogs })
            }
          })
          .catch(_err => {
            res.status(400).json({
              message: 'No Blog Today'
            })
          })
      } else {
        return res.status(401).json({
          message: 'OOPS check your Admin ID Again'
        })
      }
    })
    .catch(_err => {
      res.json({
        message: 'Admin not found'
      })
    })
}

/* ..................Delete User and Blog............. */
const deleteUser = (req, res, next) => {
  const id = req.params.id
  const userId = req.body.userID
  Admin.find()
    .exec()
    .then(admin => {
      if (admin[0]._id === id) {
        User.findByIdAndDelete(userId)
          .then(response => {
            Blog.findOneAndDelete({ sUserID: userId })
              .then(response => {
                res.json({
                  message: 'User and user blog delete'
                })
              })
              .catch(_err => {
                res.json({
                  message: 'wrong userID check again'
                })
              })
          })
          .catch(_err => {
            res.json({
              message: 'wrong information'
            })
          })
      } else {
        return res.status(401).json({
          message: 'wrong admin id'
        })
      }
    })
    .catch(_err => {
      res.json({
        message: 'Admin not found'
      })
    })
}

/* ..................Export data from Admin........... */
module.exports = {
  signup,
  login,
  showBlog,
  deleteUser,
  users
}
