const User = require('../module/user')
const Blog = require('../module/blog')
const cloudinary = require('cloudinary').v2
cloudinary.config({
  cloud_name: 'raj-yudiz',
  api_key: '858292898849192',
  api_secret: 'DrOObnclzx_C9l0ovSdmV07XOQM',
  secure: true
})

/* .............Post Blog........... */
const postBlog = (req, res, next) => {
  User.find({ _id: req.body.userID })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: 'user not exist'
        })
      } else {
        const file = req.files.coverImage
        cloudinary.uploader.upload(file.tempFilePath, (_err, result) => {
          const blog = new Blog({
            sTitle: req.body.sTitle,
            sDesc: req.body.sDesc,
            iCoverImage: result.url,
            bPublished: req.body.bPublished,
            sUserID: req.body.userID
          })
          blog.save()
            .then(result => {
              res.status(200).json({
                new_blog: result
              })
            })
            .catch(_err => {
              res.status(400).json({
                message: 'something went wrong check information again'
              })
            })
        })
      }
    })
    .catch(_err => {
      res.status(400).json({
        message: 'wrong information'
      })
    })
}
/* .............Show Blog........... */
const showBlog = (req, res, next) => {
  Blog.aggregate([{
    $match: { bPublished: { $eq: true } }
  },
  {
    $project: {
      sTitle: 1,
      sDesc: 1,
      createdAt: 1,
      updatedAt: 1
    }
  }
  ])
    .then(publishedBlogs => {
      res.json({
        publishedBlogs
      })
    })
    .catch(_err => {
      res.status(400).json({
        message: 'oops something went wrong'
      })
    })
}
/* .............Like Blog........... */
const like = (req, res, next) => {
  const userID = req.body.userID
  const blogId = req.body.blogID
  Blog.findById(blogId)
    .exec()
    .then(blog => {
      if (blog.length < 1) {
        return res.status(401).json({
          message: 'Blog not Found'
        })
      } else {
        Blog.findByIdAndUpdate(blogId, { $addToSet: { isLike: userID } })
          .then(response => {
            res.json({
              message: 'Your Like add'
            })
          })
          .catch(_err => {
            res.json({
              message: 'wrong information'
            })
          })
      }
    })
    .catch(_err => {
      res.json({
        message: 'wrong information check your details again'
      })
    })
}

/* .............Export Data from Blog........... */
module.exports = {
  postBlog,
  showBlog,
  like
}
